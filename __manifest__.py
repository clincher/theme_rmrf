{
  'name': 'RMRF theme',
  'description': 'A theme for RMRF company.',
  'version': '1.0',
  'author': 'Vasiliy Bolshakov',

  'data': [
    'views/layout.xml',
    'views/assets.xml',
    'views/snippets.xml',
    'views/options.xml',
  ],
  'category': 'Theme',
  'depends': ['website'],
  'installable': True
}
